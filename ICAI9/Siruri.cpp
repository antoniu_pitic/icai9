#include "Siruri.h"

void Pasareste(char SirSimplu[100], char SirPasarit[300])
{
	// "Lincoln"
	int i;
	int j = 0;

	for (i = 0; SirSimplu[i]; i++) {
		SirPasarit[j++] = SirSimplu[i];
		if (SirSimplu[i]=='a' || SirSimplu[i]=='e' ||
			SirSimplu[i]=='i' || SirSimplu[i]=='o' ||
			SirSimplu[i]=='u') {

				SirPasarit[j++] = 'p';
				SirPasarit[j++] = SirSimplu[i];
		}
	}

	SirPasarit[j++] = 0;
}

void DePasareste(char SirPasarit[100], char SirDePasarit[300])
{
	int i;
	int j = 0;

	for (i = 0; SirPasarit[i]; i++) {
		SirDePasarit[j++] = SirPasarit[i];
		if (SirPasarit[i] == 'a' || SirPasarit[i] == 'e' ||
			SirPasarit[i] == 'i' || SirPasarit[i] == 'o' ||
			SirPasarit[i] == 'u') {

			i += 2;
		}
	}

	SirDePasarit[j++] = 0;
}
